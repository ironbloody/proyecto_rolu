import tkinter as tk
from tkinter import ttk

class plataforma:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("Plataformas")
        self.root.resizable(width=0, height=0)

        # toplevel modal

        self.root.transient(root)

        #
        self.__config_treeview_plataforma()
        self.__config_buttons_plataforma()

    def __config_treeview_plataforma(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Plataforma")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 300, width = 300, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_plataforma()
        self.root.after(0, self.llenar_treeview_plataforma)

    def __config_buttons_plataforma(self):
        tk.Button(self.root, text="Insertar Plataforma",
            command = self.__insertar_plataforma).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar Plataforma",
            command = self.__modificar_plataforma).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar Plataforma",
            command = self.__eliminar_plataforma).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_plataforma(self):
        sql = """select * from plataforma;"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1]), iid = i[0])
            self.data = data

    def __insertar_plataforma(self):
        insertar_plataforma(self.db, self)

    def __modificar_plataforma(self):
        if(self.treeview.focus() != ""):
            sql = """select * from plataforma where id_plataforma = %(id_plataforma)s;"""

            row_data = self.db.run_select_filter(sql, {"id_plataforma": self.treeview.focus()})[0]
            modificar_plataforma(self.db, self, row_data)

    def __eliminar_plataforma(self):
        sql = "delete from plataforma where id_plataforma = %(id_plataforma)s;"
        self.db.run_sql(sql, {"id_plataforma": self.treeview.focus()})
        self.llenar_treeview_plataforma()

class insertar_plataforma:
    def __init__(self, db, padre):
        self.padre = padre
        self.db = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Insertar Plataforma")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 20)

    def __config_entry(self):
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 10, width = 80, height = 20)

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert plataforma (nombre_plataforma)
            values (%(nombre_plataforma)s);"""
        self.db.run_sql(sql, {"nombre_plataforma": self.entry_nombre.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_plataforma()

class modificar_plataforma:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Modificar plataforma")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 20)

    def config_entry(self):#Se configuran los inputs
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 10, width = 80, height = 20)
        self.entry_nombre.insert(0, self.row_data[1])

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update plataforma set nombre_plataforma = %(nombre_plataforma)s where id_plataforma = %(id_plataforma)s;"""
        self.db.run_sql(sql, {"nombre_plataforma": self.entry_nombre.get(), "id_plataforma": int(self.row_data[0])})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_plataforma()
