import tkinter as tk
from tkinter import ttk

class idioma:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("Idiomas")
        self.root.resizable(width=0, height=0)

        # toplevel modal

        self.root.transient(root)

        #
        self.__config_treeview_idioma()
        self.__config_buttons_idioma()

    def __config_treeview_idioma(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Idiomas")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 300, width = 300, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_idioma()
        self.root.after(0, self.llenar_treeview_idioma)

    def __config_buttons_idioma(self):
        tk.Button(self.root, text="Insertar Idioma",
            command = self.__insertar_idioma).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar Idioma",
            command = self.__modificar_idioma).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar Idioma",
            command = self.__eliminar_idioma).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_idioma(self):
        sql = """select * from idioma;"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1]), iid = i[0])
            self.data = data

    def __insertar_idioma(self):
        insertar_idioma(self.db, self)

    def __modificar_idioma(self):
        if(self.treeview.focus() != ""):
            sql = """select * from idioma where id_idioma = %(id_idioma)s;"""

            row_data = self.db.run_select_filter(sql, {"id_idioma": self.treeview.focus()})[0]
            modificar_idioma(self.db, self, row_data)

    def __eliminar_idioma(self):
        sql = "delete from idioma where id_idioma = %(id_idioma)s;"
        self.db.run_sql(sql, {"id_idioma": self.treeview.focus()})
        self.llenar_treeview_idioma()

class insertar_idioma:
    def __init__(self, db, padre):
        self.padre = padre
        self.db = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Insertar Idioma")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 20)

    def __config_entry(self):
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 10, width = 80, height = 20)

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert idioma (nombre)
            values (%(nombre)s);"""
        self.db.run_sql(sql, {"nombre": self.entry_nombre.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_idioma()

class modificar_idioma:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Modificar Idioma")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 20)

    def config_entry(self):#Se configuran los inputs
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 10, width = 80, height = 20)
        self.entry_nombre.insert(0, self.row_data[1])

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update idioma set nombre = %(nombre)s where id_idioma = %(id_idioma)s"""
        self.db.run_sql(sql, {"nombre": self.entry_nombre.get(), "id_idioma": int(self.row_data[0])})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_idioma()
